package com.company;

public class Main {
    private long getFactorial(final long digit) throws Exception {
    // prosze zaimplementowac algorytm wyliczania silni
    // prosze dodac obsluge nastepujacych bledow:
    // * zabezpieczenie przed wprowadzaniem ujemnych liczb
    // * zabezpieczenie przed liczbami, dla ktorych silnia nie zostanie policzona (long).
        if (digit < 0 ) {
            System.out.println("Podano liczbe ujemna.");
            throw new Exception();
        } else if (digit > 22) {
            System.out.println("Podano za dużą liczbe.");
            throw new Exception();
        }
        long result = 0;
        if (digit < 1) {
            return 1;
        } else {
            result = digit * getFactorial(digit - 1);
            return result;
        }
    }
    public static void main(String[] args) {
        long digit = 40;
        // metoda main powinna wyswietlac rowniez informacje na standardowym wyjsciu gdy wystapia bledy
        // w metodzie getFactorial - prosze zmodyfikowac kod.
        Main main = new Main();
        try {
            long factorial = main.getFactorial(digit);
            System.out.printf("Silnia(%d) = %d \n", digit, factorial);
        } catch (Exception e) {
            System.out.println("Nie można policzyć silni.");
        }
    }
}

